package servise_authorization

import (
	"errors"
	"sync"
)

type service struct {
	auto_number []int
	mu          sync.Mutex
}

func New() *service {
	return &service{
		auto_number: []int{},
	}
}

func (s *service) InitNumber(id int) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	exist := contains(id, s.auto_number)
	positiveNumber := checkNegativeNumber(id)
	if exist {
		return errors.New("Car number already exists")
	} else if positiveNumber {
		return errors.New("Car number is negative. Number should be > 0.")
	} else {
		s.auto_number = append(s.auto_number, id)
		return nil
	}
}

func (s *service) CheckNumber(id int) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	exist := contains(id, s.auto_number)
	if exist {
		return nil
	} else {
		return errors.New("Unauthorized car number")
	}

}

func contains(id int, s []int) bool {
	for _, num := range s {
		if num == id {
			return true
		}
	}
	return false
}

func checkNegativeNumber(id int) bool {
	if id < 0 {
		return true
	} else {
		return false
	}
}
