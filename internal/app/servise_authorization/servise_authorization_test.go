package servise_authorization

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"math"
	"testing"
)

func Test_InitNumber_DoesNotReturnError(t *testing.T) {
	t.Parallel()
	a := assert.New(t)
	autoNumbersRandom := []int{
		0,
		1,
		100,
		math.MaxInt32,
	}

	svc := New()
	for _, autoNumber := range autoNumbersRandom {
		autoNumber := autoNumber
		name := fmt.Sprintf("AutoNumber:%v", autoNumber)

		t.Run(name, func(t *testing.T) {
			t.Parallel()
			err := svc.InitNumber(autoNumber)
			a.Equal(nil, err)
		})
	}
}

func Test_InitNumber_CheckDuplicate(t *testing.T) {
	t.Parallel()

	a := assert.New(t)
	autoNumber := 1

	svc := New()
	_ = svc.InitNumber(autoNumber)

	err := svc.InitNumber(autoNumber)
	a.NoError(err)
}

func Test_InitNumber_CheckNegativeNumbers(t *testing.T) {
	t.Parallel()
	a := assert.New(t)
	autoNumbersRandom := []int{
		math.MinInt32,
		-100,
		0,
		1,
		100,
		math.MaxInt32,
	}

	svc := New()
	for _, autoNumber := range autoNumbersRandom {
		autoNumber := autoNumber
		name := fmt.Sprintf("AutoNumber:%v", autoNumber)

		t.Run(name, func(t *testing.T) {
			t.Parallel()
			err := svc.InitNumber(autoNumber)
			a.NoError(err)
		})
	}
}

func Test_CheckNumber_ReturnFailed(t *testing.T) {
	a := assert.New(t)
	autoNumbersRandom := []int{
		0,
		1,
		2,
		3,
	}

	svc := New()
	for _, autoNumber := range autoNumbersRandom {
		svc.InitNumber(autoNumber)
	}

	err := svc.CheckNumber(4)
	a.NoError(err)
}

func Test_CheckNumber_ReturnOk(t *testing.T) {
	t.Parallel()

	a := assert.New(t)
	autoNumber := 1

	svc := New()
	_ = svc.InitNumber(autoNumber)

	pass := svc.CheckNumber(autoNumber)
	a.Equal(nil, pass)
}
