// +build e2e

package test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"math"
	"net/http"
	"testing"
)

const host = "http://localhost:9000"

func Test_CheckAutoNumber_Subscription(t *testing.T) {
	t.Parallel()
	a := assert.New(t)

	autoNumbers := []int{
		0,
		1,
		math.MaxInt32,
	}

	for _, autoNumber := range autoNumbers {
		autoNumber := autoNumber
		name := fmt.Sprintf("AutoNumber:%v", autoNumber)

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			statusCode, message, err := postMethodSubscribe(host+"/v1/subscription", autoNumber)

			a.Equal(nil, err)
			a.Equal(200, statusCode)
			a.Equal("success - true", message)
		})
	}
}

func Test_DuplicateAutoNumber_Subscription(t *testing.T) {
	t.Parallel()

	a := assert.New(t)
	autoNumber := 100

	_, _, _ = postMethodSubscribe(host+"/v1/subscription", autoNumber)
	statusCode, message, err := postMethodSubscribe(host+"/v1/subscription", autoNumber)

	a.Equal(nil, err)
	a.Equal(409, statusCode)
	a.Contains(message, "success - false")
}

func Test_NegativeAutoNumber_Subscription(t *testing.T) {
	t.Parallel()

	a := assert.New(t)
	autoNumber := -1

	statusCode, message, err := postMethodSubscribe(host+"/v1/subscription", autoNumber)

	a.Equal(nil, err)
	a.Equal(409, statusCode)
	a.Contains(message, "success - false")
}

func Test_CheckAutoNumber_Check(t *testing.T) {
	t.Parallel()
	a := assert.New(t)

	autoNumbers := []int{
		0,
		1,
	}

	for _, autoNumber := range autoNumbers {
		autoNumber := autoNumber
		name := fmt.Sprintf("AutoNumber:%v", autoNumber)

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			statusCode, message, err := postMethodSubscribe(host+"/v1/check", autoNumber)

			a.Equal(nil, err)
			a.Equal(200, statusCode)
			a.Equal("pass - true", message)
		})
	}
}

func postMethodSubscribe(host string, autoNumber int) (int, string, error) {
	requestBody, err := json.Marshal(map[string]int{
		"auto_number": autoNumber,
	})

	if err != nil {
		return 0, "", err
	}

	result, err := http.Post(host, "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		return 0, "", err
	}

	body, err := ioutil.ReadAll(result.Body)
	defer result.Body.Close()

	return result.StatusCode, string(body), err
}
