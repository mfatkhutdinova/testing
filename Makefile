build:
	mkdir -p bin
	go build -o bin/service_authorization cmd/service_authorization/main.go

run:
	go run cmd/service_authorization/main.go