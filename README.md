# Решение
Интеграционные и unit тесты написаны для проекта [Д/з: сервис авторизации для парковки](https://git.ozon.dev/mfatkhutdinova/23_transition_to_go).  
**Unit тесты** - /internal/app/servise_authorization  
**Интеграционные тесты** - /test/authorization_test

# Домашнее задание

### Пункт 0.
Проверьте уже написанные вами тесты

### Пункт 1.
Напишите функциональные unit тесты для одного из пакетов:
* [Д/з к лекции «Performance, Profiling, Debugging»](https://git.ozon.dev/practice/2.7-performance/-/blob/master/README.md)
* [Д/з к лекции «Редакторы: обзор эффективных для Ozon»](https://git.ozon.dev/practice/22_ide/-/blob/master/homework/README.md) 
* [Д/з: сервис авторизации для парковки](https://git.ozon.dev/dkolesnik/23_transition_to_go/-/blob/master/README.md) 

### Пункт 2.
Напишите функциональные интеграционные тесты для одного end-point к репозиториям:
* [Практическое задание к лекции «Редакторы: обзор эффективных для Ozon»](https://git.ozon.dev/practice/22_ide/-/blob/master/practice/README.md)
* [Д/з к лекции «Редакторы: обзор эффективных для Ozon»](https://git.ozon.dev/practice/22_ide/-/blob/master/homework/README.md)
* [Д/з: сервис авторизации для парковки](https://git.ozon.dev/dkolesnik/23_transition_to_go/-/blob/master/README.md)
 
### Пункт 3.
Пишите тесты для своих будущих приложений

## Критерии выполнения домашнего задания

* Наименование тестов (По названию нужно понять, что происходит в тесте)
* Желательна структура AAA (Arrange - Act - Assert)
* Нет god тестов (один тест проверяет все) => есть много маленьких тестов, которые проверяют только одну функциональность
* Читаемые сообщения об ошибке, в случае падения теста
* Тесты атомарны (тесты не влияют друг на друга, каждый тест можно запустить отдельно от остальных)
* Тесты не деградируют (их можно запускать хоть 1000 раз подряд)